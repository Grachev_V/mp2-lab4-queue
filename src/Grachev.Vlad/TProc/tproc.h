// �����, ����������� ���������

#ifndef _TPROC_H_
#define _TPROC_H_

#include <ctime>
#include <stdlib.h>
#include "tqueue.h"

#define defpipe_size 10
#define defperf 50

class TProc
{
	private:
		int s = 10;
		TQueue pipeline; // �������, �������� �������
		int perf; // ����������� ���������� ������� �� ������� �����
	public:
		TProc(int psize = defpipe_size, int pf = defperf) : pipeline(psize), perf(pf) { srand(time(0)); };
		bool CanTakeTask(); // ������� ����� ��� ���������� �������
		bool IsBusy(); // ������� �������, ������������ �� ����������
		bool NewTask(long task_id); // ��������� ������� �� ���������
		bool IsTaskDone(); // ��������� �������
};
#endif