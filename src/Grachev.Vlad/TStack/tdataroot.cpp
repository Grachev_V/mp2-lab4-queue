// Динамические структуры данных - базовый (абстрактный) класс - версия 3.2
//   память выделяется динамически или задается методом SetMem

#include <stdio.h>
#include "tdataroot.h"

	/*-------------------------------------------*/
	
TDataRoot :: TDataRoot (int Size): TDataCom(), MemSize(Size), DataCount(0){
	if(Size == 0) // память будет выделена методом SetMem
	{
		pMem = NULL; 
		MemType = MEM_RENTER;
	}
	else // память будет выделяться самим объектом
	{	
		pMem = new TElem[MemSize];
		MemType = MEM_HOLDER;
	}	
}	

	/*-------------------------------------------*/
	
TDataRoot :: ~TDataRoot()
{
	if(MemType == MEM_HOLDER)
		delete[] pMem;
	pMem = NULL;
}	
	
	/*-------------------------------------------*/

void TDataRoot :: SetMem(void *p, int Size) // задание памяти
{
	if(MemType == MEM_HOLDER)
		delete pMem; // информация не сохраняется
	MemType = MEM_RENTER;
	pMem = (TElem *)p;
	MemSize = Size;
}

	/*-------------------------------------------*/

bool TDataRoot :: IsEmpty() const  // контроль пустоты СД
{
	return DataCount == 0;
} 	

/*-------------------------------------------*/

bool TDataRoot :: IsFull() const // контроль переполнения СД
{
	return DataCount == MemSize;
}