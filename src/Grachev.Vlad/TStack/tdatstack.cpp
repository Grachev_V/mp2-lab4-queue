// Динамические структуры данных - стек

#include <stdio.h>
#include "tdatstack.h"

void TStack :: Push(const TData &Val) // положить элемент в стек
{
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else
		if(IsFull())
			SetRetCode(DataFull);
	else
	{
		Hi = GetNextIndex(Hi);
		pMem[Hi] = Val;
		DataCount++;
	}	
}

	/*-------------------------------------------*/

TData TStack :: Pop() // взять элемент из стека
{
	TData temp = -1;
	if(pMem == NULL)
		SetRetCode(DataNoMem);
	else
		if(IsEmpty())
			SetRetCode(DataEmpty);
	else
	{
		temp = pMem[Hi--];
		DataCount--;
	}
	return temp;
}

	/*-------------------------------------------*/

TData TStack::TopElem() // посмотреть значение вершины стека
{
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else
		if (IsEmpty())
			SetRetCode(DataEmpty);
		else
			return pMem[Hi];
}

	/*-------------------------------------------*/

int TStack :: GetNextIndex(int index) {return ++index;} // получить следующее значение индекса

int  TStack :: IsValid()// тестирование структуры
{
	int res = 0;
	if (pMem == nullptr)
		res++;
	if (MemSize < DataCount)
		res += 2;
	return res;
}
	/*-------------------------------------------*/

void TStack :: Print() // печать значений стека
{
	for(int i = 0; i < DataCount; i++)
		printf("%d", pMem[i]);
	printf("\n");
}	
