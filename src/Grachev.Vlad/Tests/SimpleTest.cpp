//#define SMPL_TEST
#ifdef SMPL_TEST

#include <iostream>
#include "tqueue.h"

using namespace :: std;

int main()
{
	TQueue q(5);
	int temp;

	while (!q.IsFull())
	{
		cin >> temp;
		q.Put(temp);
	}
	q.Get();
	if (q.IsFull())
		cout << "Queue is full" << endl;
	q.Put(6);
	while (!q.IsEmpty())
		cout << q.Get();
	cout << endl;
	return 0;
}

#endif