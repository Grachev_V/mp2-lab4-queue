#define USE_TESTS

#ifdef USE_TESTS

#include <gtest/gtest.h>

#include "tqueue.h"

TEST(TQueue, can_create_queue)
{
	ASSERT_NO_THROW(TQueue q1);
}

TEST(TQueue, can_create_queue_with_set_size)
{
	ASSERT_NO_THROW(TQueue q1(5));
}

TEST(TQueue, new_queue_is_empty)
{
	TQueue q1;
	EXPECT_EQ(true, q1.IsEmpty());
}

TEST(TQueue, can_put_elem_in_queue)
{
	TQueue q1;
	int elem = 1;
	ASSERT_NO_THROW(q1.Put(elem));
}

TEST(TQueue, queue_with_elem_isnt_empty)
{
	TQueue q1;
	int elem = 1;
	q1.Put(elem);
	EXPECT_EQ(false, q1.IsEmpty());
}

TEST(TQueue, cant_put_in_full_queue)
{
	TQueue q1(5);
	int i = 1;
	while (!q1.IsFull())
		q1.Push(i++);
	q1.Push(i + 1);
	EXPECT_EQ(DataFull, q1.GetRetCode());
}


TEST(TQueue, can_get_elem_from_queue)
{
	TQueue q1;
	int elem1 = 1, elem2;
	q1.Push(elem1);
	elem2 = q1.Get();
	EXPECT_EQ(elem1, elem2);
}

TEST(TQueue, cant_get_from_empty_queue)
{
	TQueue q1;
	q1.Get();
	EXPECT_EQ(DataEmpty, q1.GetRetCode());
}

TEST(TQueue, get_returns_first_placed_elem)
{
	TQueue q1;
	int elem1 = 1, elem2 = 2, elem3 = 3, res;
	q1.Put(elem1); q1.Put(elem2); q1.Put(elem3);
	res = q1.Get();
	EXPECT_TRUE(res == elem1);
}

TEST(TQueue, full_queue_after_get_isnt_full)
{
	TQueue q1;
	int i = 1;
	while (!q1.IsFull())
		q1.Push(i++);
	bool isf = q1.IsFull();
	int val = q1.Pop();
	EXPECT_TRUE(isf != q1.IsFull());
}

TEST(TQueue, can_put_elem_in_full_queue_after_get)
{
	TQueue q1;
	int i = 1, temp;
	while (!q1.IsFull())
		q1.Put(i++);
	temp = q1.Get() + 1;
	q1.Put(temp);
	while (!q1.IsEmpty())
		i = q1.Get();
	EXPECT_EQ(temp, i);
}
#endif
