// ������������ ��������� ������ - �������

#ifndef _TQUEUE_H_
#define _TQUEUE_H_

#include "tdatstack.h"

class TQueue : public TStack
{
protected:
	int Li; // ������ ������� �������� ���������
	virtual int GetNextIndex(int Index);
public:
	TQueue(int Size = DefMemSize) : TStack(Size)
	{
		Li = -1;
	}
	virtual void Put(const TData &Val); // �������� ������� � �������
	virtual TData Get(void); // ����� ������� �� �������
};
#endif